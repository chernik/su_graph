#include <iostream>
#include <fstream>
#include <cstdlib>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

typedef unsigned char uchar;

class Graph {
	private:
	uchar* buf;
	
	void drawGraph(){
		int f = open("/dev/fb0", O_WRONLY);
		if(f < 0){
			perror("No open");
		}
		int r = write(f, buf, 4*graphSize[0]*graphSize[1]);
		if(r < 0){
			perror("No write");
		}
		if(r != 4*graphSize[0]*graphSize[1]){
			std::cout << r << " " << 4*graphSize[0]*graphSize[1] << "\n";
		}
		close(f);
	}
	
	public:
	int txtSize[2] = {100, 37}; // Размер экрана в символах
	int graphSize[2] = {1176, 885}; // Размер экрана в пикселях
	
	Graph(){
		buf = new uchar[4*graphSize[0]*graphSize[1]]; // Буфер, куда мы пиксели рисуем
	}
	
	inline void putPixel(int x, int y, uchar r, uchar g, uchar b){ // Нарисовать один пиксель
		buf[0 + (x + y * graphSize[0]) * 4] = b;
		buf[1 + (x + y * graphSize[0]) * 4] = g;
		buf[2 + (x + y * graphSize[0]) * 4] = r;
		buf[3 + (x + y * graphSize[0]) * 4] = 0;
	}
	
	void testGradient(uchar g){ // Тестируем градиент
		for(int y = 0; y < graphSize[1]; y++){
			for(int x = 0; x < graphSize[0]; x++){
				uchar r = x * 255 / graphSize[0];
				uchar b = y * 255 / graphSize[1];
				putPixel(x, y, r, g, b);
			}
		}
	}
	
	void fill(uchar r, uchar g, uchar b){ // Заполняем одним цветом
		for(int x = 0; x < graphSize[0]; x++){
			for(int y = 0; y < graphSize[1]; y++){
				putPixel(x, y, r, g, b);
			}
		}
	}
	
	void moveCursor(int x, int y){ // Перемещаем текстовый курсор куда надо
		char str[255];
		sprintf(str, "\033[%d;%dH", y, x);
		write(0, str, strlen(str));
	}
	
	void clear(){ // Очищает экран
		system("clear");
		//write(0, "\033[2L", 4);
		//moveCursor(1, 1);
	}
	
	~Graph(){
		delete buf;
	}
	
	void draw(){ // Вывести буфер на экран
		moveCursor(1, txtSize[1]);
		drawGraph();
		moveCursor(txtSize[0], 1);
		moveCursor(1, 1);
		moveCursor(txtSize[0], txtSize[1]);
	}
	
	static void example(){ // Пример
		Graph graph;
		graph.clear(); // Очищаем экран
		usleep(500000); // Ждем полсекунды, чтобы глюков не было
		for(int g = 0; g < 256; g+=5){
			graph.testGradient(g); // Рисуем градиент
			graph.draw(); //Выводим
		}
		for(int g = 0; g < 50; g++){
			graph.fill((g%2)*255, (1-(g%2))*255, 0); // Заполняем одним цветом
			graph.draw();
			usleep(20000); // Ждем 0.02 секунды
		}
		for(int i = 5; i > 0; i--){
			graph.moveCursor(10, 10);
			std::cout << "Exit with " << i << " s" << std::flush;
			usleep(1000000); // Ждем секунду
		}
		graph.clear();
	}
};

int main(){
	Graph::example();	
	return 0;
}
